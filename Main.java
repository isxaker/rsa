package ru.msg.encrypter;

import java.io.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
* Created by IntelliJ IDEA.
* User: mikhail
* Date: Oct 4, 2010
* Time: 10:46:13 PM
* To change this template use File | Settings | File Templates.
*/
public class Main {
    public static void main(String args[]) {

//        args = new String[5];
//        args[0] = new String("-gen");
//        args[1] = new String("1024");
//        args[2] = new String("E:\\modulus.txt");
//        args[3] = new String("E:\\public.txt");
//        args[4] = new String("E:\\private.txt");

//        args = new String[5];
//        args[0] = new String("-e");
//        args[1] = new String("E:\\message.txt");
//        args[2] = new String("E:\\result.txt");
//        args[3] = new String("E:\\modulus.txt");
//        args[4] = new String("E:\\public.txt");

  /*      args = new String[5];
        args[0] = new String("-d");
        args[1] = new String("E:\\result.txt");
        args[2] = new String("E:\\message2.txt");
        args[3] = new String("E:\\modulus.txt");
        args[4] = new String("E:\\private.txt");*/



        if (args != null && args.length > 0) {
            // generate keys
            if (args.length == 5 && args[0].equals("-gen")) {
                final List<BigInteger> keys = RsaEncrypter.generateKeys(Integer.parseInt(args[1]));
                writeKeys(keys, args);
                return;
            }
            // encrypting or decrypting
            if (args.length == 5 && (args[0].equals("-e") || args[0].equals("-d"))) {
                final List<BigInteger> keys = readKeys(args[3], args[4]);
                final byte[] message = readMessage(args[1]);
                final byte[] res = RsaEncrypter.encrypt(message, keys);
                writeMessage(res, args[2]);
                return;
            }
        } 


        PrintError();
        return;
    }

    private static void writeMessage(final byte[] message, final String path) {
        try {
            final OutputStream outputStream = new FileOutputStream(path);
            outputStream.write(message);
            outputStream.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private static byte[] readMessage(final String path) {
        try {
            final File file = new File(path);
            final InputStream inputStream = new FileInputStream(file);
            final byte[] data = new byte[(int)file.length()];
            inputStream.read(data);
            inputStream.close();

            return data;
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return new byte[]{};

    }

    private static List<BigInteger> readKeys(final String modulusPath, final String keyPath) {
        final List<BigInteger> keys = new ArrayList<BigInteger>();
        keys.add(readBigInteger(modulusPath));
        keys.add(readBigInteger(keyPath));
        return keys;
    }

    private static void writeKeys(final List<BigInteger> keys, String[] args) {
        for (int i =0; i < 3; i++) {
            writeBigInteger(keys.get(i), args[i+2]);
        }
    }

    private static void writeBigInteger(final BigInteger bigInteger, final String path) {
        try {
            final byte[] data = bigInteger.toByteArray();
            final OutputStream outputStream = new FileOutputStream(path);
            outputStream.write(data);
            // dispose all the resources after using them.
            outputStream.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

     private static BigInteger readBigInteger(final String path) {
        try {
            final File file = new File(path);
            final InputStream inputStream = new FileInputStream(file);
            final byte[] data = new byte[(int)file.length()];
            inputStream.read(data);
            inputStream.close();
            final BigInteger res = new BigInteger(data);
            return res;
        } catch (final Exception e) {
            e.printStackTrace();
        }

        return null;

    }


    public static void PrintError() {
        System.out.println("No parameters\n" +
                "You must enter the parameters of the program\n" +
                "ru.msg.encrypter.RsaEncrypter " + "-gen(generating private and public keys)" +
                "numBit pathForModulus pathForPublicKey pathForPrivateKey \n" +
                "-e(encrypt message) pathForMessage pathForResult pathForModulus pathForPublicKey\n" +
                "-d(decrypt message) pathForMessage pathForResult pathForModulus pathForPrivateKey\n");
    }

}
