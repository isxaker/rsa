package ru.msg.encrypter;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: mikhail
 * Date: Oct 5, 2010
 * Time: 9:21:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class RsaEncrypter {

    // generating keys
    public static List<BigInteger> generateKeys(final int N) {
        final SecureRandom random = new SecureRandom();
        final BigInteger one = new BigInteger("1");

        final BigInteger p = BigInteger.probablePrime(N / 2, random);
        final BigInteger q = BigInteger.probablePrime(N / 2, random);
        final BigInteger phi = (p.subtract(one)).multiply(q.subtract(one));

        final BigInteger modulus = p.multiply(q);
        final BigInteger publicKey = new BigInteger("65537");     // common value in practice = 2^16 + 1
        final BigInteger privateKey = publicKey.modInverse(phi);

        final List<BigInteger> res = new ArrayList();
        res.add(modulus);
        res.add(publicKey);
        res.add(privateKey);
        return res;
    }

    // cryptography
    public static byte[]  encrypt(final byte[] message,final  List<BigInteger> keys) {
        final BigInteger messageBI = new BigInteger(message);
        final byte[] res = messageBI.modPow(keys.get(1), keys.get(0)).toByteArray();
        return res;
    }

    public static byte[] rsa(final byte[] message, final List<BigInteger> keys) {
        final BigInteger messageBI = new BigInteger(message);
        final byte[] res = messageBI.modPow(keys.get(1), keys.get(0)).toByteArray();
        return res;
    }

    public static void checkSignature(final byte[] currentMessage, final byte[] originalMessage) {
        if (java.util.Arrays.equals(currentMessage, originalMessage)) {
            System.out.println("Signature is valid");
        } else {
            System.out.println("Signature is not valid");
        }
    }




}
